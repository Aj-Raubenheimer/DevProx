<!doctype html>
<html>

<head>
    <title>HTML FORM</title>
</head>

<body>

    <h1>Add User Details</h1>

    <form method="POST" action=" ">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" placeholder="name" required>

        <label for="surname">Surname:</label>
        <input type="text" id="surname" name="surname" placeholder="surename" required>

        <label for="idno">ID No:</label>
        <input type="text" id="idno" name="idno" placeholder="id nr" required>
        
        <label for="dob">Date of Birth:</label>
        <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">

        <button type="submit" name="submit">Submit</button>
        <button type="reset" name="cancel">Cancel</button>
    </form>

</body>

</html>

<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Process form submission and insert data into MySQL
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $idno = $_POST['idno'];
        $dob = $_POST['dob'];
        
        // Validate input fields
        if (!preg_match("/^[a-zA-Z ]*$/", $name) || !preg_match("/^[a-zA-Z ]*$/", $surname)) {
            die("Name and surname can only contain letters and spaces.");
        }
        if (!is_numeric($idno) || strlen($idno) !== 13) {
            die("ID No must be a 13-digit number.");
        }
        if (!preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $dob)) {
            die("Date of birth must be in the format dd/mm/YYYY.");
        }
        
        // Connect to database
        $db = new mysqli('db', 'devprox', 'password', 'devprox_test_1');
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        
        // Check for duplicate ID No
        $sql = "SELECT * FROM people WHERE idno = '$idno'";
        $result = $db->query($sql);
        if ($result->num_rows > 0) {
            die("ID No already exists in the database.");
        }
        
        // Insert record into database
        $sql = "INSERT INTO people (name, surname, idno, dob) VALUES ('$name', '$surname', '$idno', '$dob')";
        if ($db->query($sql) === TRUE) {
            echo "Record inserted successfully.";
        } else {
            echo "Error inserting record: " . $db->error;
        }
        
        // Close database connection
        $db->close();

    } else {
    // This is the first page load, do nothing
    }
?>
