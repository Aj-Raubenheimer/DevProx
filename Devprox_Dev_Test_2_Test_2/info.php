<?php

// Create arrays of names and surnames
$names = array("Emma", "Olivia", "Ava", "Isabella", "Sophia", "Mia", "Charlotte", "Amelia", "Harper", "Evelyn", "Abigail", "Emily", "Elizabeth", "Mila", "Ella", "Avery", "Sofia", "Camila", "Aria", "Scarlett");
$surnames = array("Smith", "Johnson", "Brown", "Taylor", "Miller", "Wilson", "Moore", "Anderson", "Jackson", "Harris", "Martin", "Lee", "Davis", "Parker", "Green", "Evans", "Edwards", "Hall", "Baker", "Carter");

// Function to generate a random date of birth between two dates
function randomDate($start_date, $end_date) {
    $start_timestamp = strtotime($start_date);
    $end_timestamp = strtotime($end_date);
    $random_timestamp = mt_rand($start_timestamp, $end_timestamp);
    return date('Y-m-d', $random_timestamp);
}

// Generate random names, ages, and birthdates and write to CSV file
$entries = array();
for ($i = 0; $i < 1100000; $i++) {
    $name = $names[array_rand($names)];
    $surname = $surnames[array_rand($surnames)];
    $initial = substr($name, 0, 1);
    $age = mt_rand(1, 100);
    $birthdate = randomDate('1900-01-01', '2023-12-31');
    $entry = array($name, $surname, $initial, $age, $birthdate);
    $entries[] = $entry;
}

// Remove duplicates
$unique_entries = array_unique($entries, SORT_REGULAR);

// Write to CSV file
$filename = 'backupstore.csv';
$fp = fopen($filename, 'w');
foreach ($unique_entries as $entry) {
    fputcsv($fp, $entry);
}
fclose($fp);

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="output.csv";');
readfile($filename);

?>