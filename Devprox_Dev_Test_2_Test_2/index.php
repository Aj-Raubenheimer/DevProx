<!doctype html>
<html>

<head>
    <title>CSV Import</title>
</head>

<body>

    <h1>Import CSV</h1>

    <form method="POST" action=" ">
        <input type="file" name="csv_file">
        <input type="submit" name="submit" value="Upload">
    </form>

</body>

</html>

<?php
// Check if form has been submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['submit'])) {
    // Connect to MySQL database
    $conn = new mysqli('db', 'devprox', 'password', 'devprox_test_2');
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // Read CSV file and insert data into MySQL database

    if ($_FILES['csv_file']['error'] == UPLOAD_ERR_OK) {
        $file = fopen($_FILES['csv_file']['tmp_name'], 'r');
        while (($line = fgetcsv($file)) !== false) {
        $name = $conn->real_escape_string($line[0]);
        $surname = $conn->real_escape_string($line[1]);
        $initial = $conn->real_escape_string($line[2]);
        $age = $conn->real_escape_string($line[3]);
        $birthdate = $conn->real_escape_string($line[4]);
        $sql = "INSERT INTO names (name, surname, initial, age, birthdate) VALUES ('$name', '$surname', '$initial', '$age', '$birthdate')";
        $conn->query($sql);
        }
        fclose($file);
        echo "CSV file uploaded successfully";
    } else {
        echo "Error uploading CSV file";
    }
    
    // Close MySQL database connection
    $conn->close();
    }
} else {
    // This is the first page load, do nothing
    }
?>